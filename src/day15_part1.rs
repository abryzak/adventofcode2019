use std::cmp::{max, min};
use std::collections::{HashMap, VecDeque};
use std::io::BufRead;

macro_rules! intcodedebug {
    () => {
        //        println!()
    };
    ($($arg:tt)*) => {{
        //        println!($($arg)*);
    }};
}

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<i64>, String> {
    let mut vals: Vec<i64> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

enum ParameterMode {
    PositionMode,
    ImmediateMode,
    RelativeMode,
}

fn param_mode(mode_int: u32) -> Result<ParameterMode, String> {
    let mode = match mode_int {
        0 => ParameterMode::PositionMode,
        1 => ParameterMode::ImmediateMode,
        2 => ParameterMode::RelativeMode,
        _ => return Err(format!("Unrecognized parameter mode: {}", mode_int)),
    };
    Ok(mode)
}

enum ComputationResult {
    Finished,
    RequiresInput,
    Output(i64),
}

#[derive(Clone, Debug, PartialEq)]
struct Computer {
    ip: usize,
    relative_base: usize,
    memory: Vec<i64>,
    inputs: VecDeque<i64>,
}

impl Computer {
    fn validate_address(&self, address: i64) -> Result<usize, String> {
        if address < 0 {
            return Err(format!("Out of bounds memory access: {}", address));
        }
        Ok(address as usize)
    }

    fn read_mem(&self, address: usize) -> i64 {
        let value = if address >= self.memory.len() {
            0
        } else {
            self.memory[address]
        };
        intcodedebug!("read {}: {}", address, value);
        value
    }

    fn write_mem(&mut self, address: usize, value: i64) {
        // MAYBE store separate HashMap of extended addresses
        while address >= self.memory.len() {
            self.memory.push(0);
        }
        intcodedebug!("write {} to {}", value, address);
        self.memory[address] = value;
    }

    fn next_instruction(&self) -> Result<u32, String> {
        let instruction_value: i64 = self.read_mem(self.ip);
        if instruction_value < 0 {
            return Err(format!("Invalid instruction {}", instruction_value));
        }
        Ok(instruction_value as u32)
    }

    fn read_value_param(&self, param_idx: usize, instruction: u32) -> Result<i64, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                let address = self.validate_address(raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::ImmediateMode => raw_value,
        };
        Ok(value)
    }

    fn read_address_param(&self, param_idx: usize, instruction: u32) -> Result<usize, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                // MAYBE this should never happen?
                let address = self.validate_address(raw_value)?;
                //                self.validate_address(self.read_mem(address))?
                address
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                address
            }
            ParameterMode::ImmediateMode => self.validate_address(raw_value)?,
        };
        Ok(value)
    }

    pub fn run(&mut self) -> Result<ComputationResult, String> {
        loop {
            intcodedebug!();
            let instruction = self.next_instruction()?;
            let opcode = instruction % 100;
            intcodedebug!(
                //                "IP: {}, instruction: {}, opcode: {}, rb: {}, param1: {}, param2: {}, param3: {}",
                "IP: {}, instruction: {}, opcode: {} rb: {}",
                self.ip,
                instruction,
                opcode,
                self.relative_base,
                //                self.read_mem(self.ip + 1),
                //                self.read_mem(self.ip + 2),
                //                self.read_mem(self.ip + 3)
            );
            match opcode {
                1 => {
                    // add 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 + val2);
                    self.ip += 4;
                }
                2 => {
                    // mul 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 * val2);
                    self.ip += 4;
                }
                3 => {
                    // read input and store at address 1
                    let address = self.read_address_param(1, instruction)?;
                    intcodedebug!(
                        "address: {}, {}, relative base: {}",
                        address,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    let val = match self.inputs.pop_front() {
                        None => break Ok(ComputationResult::RequiresInput),
                        Some(v) => v,
                    };
                    self.write_mem(address, val);
                    self.ip += 2;
                }
                4 => {
                    // output 1
                    let val1 = self.read_value_param(1, instruction)?;
                    self.ip += 2;
                    break Ok(ComputationResult::Output(val1));
                }
                5 => {
                    // if 1 != 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 != 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                6 => {
                    // if 1 == 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 == 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                7 => {
                    // if 1 < 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 < val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                8 => {
                    // if 1 = 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 == val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                9 => {
                    // set relative base to relative base + 1
                    let val1 = self.read_value_param(1, instruction)?;
                    intcodedebug!(
                        "set relative base, instruction: {}, val: {}, raw_param: {}, relative base: {}",
                        instruction,
                        val1,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    if val1 >= 0 {
                        self.relative_base += val1 as usize;
                    } else {
                        self.relative_base -= -val1 as usize;
                    }
                    self.ip += 2;
                }
                99 => break Ok(ComputationResult::Finished),
                _ => {
                    break Err(format!(
                        "Unexpected opcode {} at address {}",
                        opcode, self.ip
                    ))
                }
            };
        }
    }
}

// END COMPUTER

const NORTH: (i64, i64) = (0, -1);
const SOUTH: (i64, i64) = (0, 1);
const WEST: (i64, i64) = (-1, 0);
const EAST: (i64, i64) = (1, 0);

const NORTH_INPUT: i64 = 1;
const SOUTH_INPUT: i64 = 2;
const WEST_INPUT: i64 = 3;
const EAST_INPUT: i64 = 4;

enum Tile {
    Start,
    Wall,
    Vacant,
    OxygenSystem,
}

const VACANT: &str = " ";
const WALL: &str = "█";
const START: &str = "o";
const OXYGEN_SYSTEM: &str = "X";
const DROID: &str = "D";

struct Droid {
    computer: Computer,
    x: i64,
    y: i64,
    tiles: HashMap<(i64, i64), Tile>,
    last_move: (i64, i64),
    opposite_input: i64,
    reverse_path: Vec<i64>,
}

impl Droid {
    pub fn new(computer: Computer) -> Self {
        let mut droid = Droid {
            computer,
            x: 0,
            y: 0,
            tiles: HashMap::new(),
            last_move: (0, 0),
            opposite_input: 0,
            reverse_path: Vec::new(),
        };
        droid.tiles.insert((0, 0), Tile::Start);
        droid
    }

    fn output_maze(&self) {
        println!("-----");
        let mut x_min = 0;
        let mut y_min = 0;
        let mut x_max = 0;
        let mut y_max = 0;
        for (x, y) in self.tiles.keys() {
            x_min = min(x_min, *x);
            y_min = min(y_min, *y);
            x_max = max(x_max, *x);
            y_max = max(y_max, *y);
        }

        for y in y_min..=y_max {
            for x in x_min..=x_max {
                if x == self.x && y == self.y {
                    print!("{}", DROID);
                    continue;
                }
                match self.tiles.get(&(x, y)) {
                    None => print!("{}", VACANT),
                    Some(Tile::Vacant) => print!("{}", VACANT),
                    Some(Tile::Wall) => print!("{}", WALL),
                    Some(Tile::Start) => print!("{}", START),
                    Some(Tile::OxygenSystem) => print!("{}", OXYGEN_SYSTEM),
                }
            }
            println!();
        }
    }

    pub fn run(&mut self) -> Result<(), String> {
        let mut shortest_path_to_oxygen = std::usize::MAX;
        loop {
            match self.computer.run()? {
                ComputationResult::Finished => {
                    break;
                }
                ComputationResult::RequiresInput => {
                    if self
                        .tiles
                        .get(&(self.x + NORTH.0, self.y + NORTH.1))
                        .is_none()
                    {
                        self.last_move = NORTH;
                        self.opposite_input = SOUTH_INPUT;
                        self.computer.inputs.push_back(NORTH_INPUT);
                    } else if self
                        .tiles
                        .get(&(self.x + SOUTH.0, self.y + SOUTH.1))
                        .is_none()
                    {
                        self.last_move = SOUTH;
                        self.opposite_input = NORTH_INPUT;
                        self.computer.inputs.push_back(SOUTH_INPUT);
                    } else if self
                        .tiles
                        .get(&(self.x + WEST.0, self.y + WEST.1))
                        .is_none()
                    {
                        self.last_move = WEST;
                        self.opposite_input = EAST_INPUT;
                        self.computer.inputs.push_back(WEST_INPUT);
                    } else if self
                        .tiles
                        .get(&(self.x + EAST.0, self.y + EAST.1))
                        .is_none()
                    {
                        self.last_move = EAST;
                        self.opposite_input = WEST_INPUT;
                        self.computer.inputs.push_back(EAST_INPUT);
                    } else {
                        if self.reverse_path.is_empty() {
                            println!("Finished exploring maze");
                            break;
                        }
                        let reverse_input = self.reverse_path.pop().unwrap();
                        self.computer.inputs.push_back(reverse_input);
                        self.opposite_input = 0;
                        match reverse_input {
                            1 => {
                                self.last_move = NORTH;
                            }
                            2 => {
                                self.last_move = SOUTH;
                            }
                            3 => {
                                self.last_move = WEST;
                            }
                            4 => {
                                self.last_move = EAST;
                            }
                            _ => return Err(format!("Unexpected reverse move: {}", reverse_input)),
                        }
                    }

                    // fun solution to manually do it
                    // self.output_maze();
                    // enable_raw_mode().expect("Failed to enable raw mode");
                    // loop {
                    //     let event = read().unwrap();
                    //     if event == Event::Key(KeyCode::Up.into()) {
                    //         self.last_move = NORTH;
                    //         self.computer.inputs.push_back(NORTH_INPUT);
                    //         break;
                    //     } else if event == Event::Key(KeyCode::Down.into()) {
                    //         self.last_move = SOUTH;
                    //         self.computer.inputs.push_back(SOUTH_INPUT);
                    //         break;
                    //     } else if event == Event::Key(KeyCode::Left.into()) {
                    //         self.last_move = WEST;
                    //         self.computer.inputs.push_back(WEST_INPUT);
                    //         break;
                    //     } else if event == Event::Key(KeyCode::Right.into()) {
                    //         self.last_move = EAST;
                    //         self.computer.inputs.push_back(EAST_INPUT);
                    //         break;
                    //     }
                    // }
                    // disable_raw_mode().expect("Failed to disable raw mode");
                }
                ComputationResult::Output(value) => {
                    match value {
                        0 => {
                            // mark next position as wall
                            intcodedebug!(
                                "Hit wall from {},{} at {},{}",
                                self.x,
                                self.y,
                                self.x + self.last_move.0,
                                self.y + self.last_move.1
                            );
                            assert!(self.opposite_input > 0);
                            self.tiles.insert(
                                (self.x + self.last_move.0, self.y + self.last_move.1),
                                Tile::Wall,
                            );
                        }
                        1 => {
                            // update position, mark as vacant
                            self.x += self.last_move.0;
                            self.y += self.last_move.1;
                            if self.opposite_input > 0 {
                                self.tiles.entry((self.x, self.y)).or_insert(Tile::Vacant);
                                self.reverse_path.push(self.opposite_input);
                            }
                        }
                        2 => {
                            // update position, mark as oxygen
                            // update position, mark as vacant
                            self.x += self.last_move.0;
                            self.y += self.last_move.1;
                            if self.opposite_input > 0 {
                                println!("Found oxygen system at {},{}", self.x, self.y);
                                self.reverse_path.push(self.opposite_input);
                                self.tiles
                                    .entry((self.x, self.y))
                                    .or_insert(Tile::OxygenSystem);
                                shortest_path_to_oxygen =
                                    min(shortest_path_to_oxygen, self.reverse_path.len());
                            }
                        }
                        _ => return Err(format!("Unexpected output value: {}", value)),
                    }
                }
            }
        }

        self.output_maze();
        println!("Shorted path from start is {}", shortest_path_to_oxygen);

        Ok(())
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let program = line.trim();
                if program.len() == 0 || program.starts_with("#") {
                    continue;
                }
                let base_memory = split_to_numbers(program)?;
                let computer = Computer {
                    ip: 0,
                    relative_base: 0,
                    memory: base_memory,
                    inputs: VecDeque::new(),
                };
                let mut droid = Droid::new(computer);
                droid.run()?;
            }
        };
    }
    Ok(())
}
