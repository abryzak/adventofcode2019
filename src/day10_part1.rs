use std::io::BufRead;

enum Entity {
    Space,
    Asteroid,
}

struct Map {
    entities: Vec<Entity>,
    cols: usize,
}

impl Map {
    pub fn new(cols: usize) -> Map {
        let entities = Vec::with_capacity(cols * cols);
        return Map {
            entities: entities,
            cols,
        };
    }

    pub fn add_row(&mut self, line: &str) -> Result<(), String> {
        if line.len() != self.cols {
            return Err(format!(
                "Different number of columns in row: {}, should be {}",
                line.len(),
                self.cols
            ));
        }
        let mut row = Vec::with_capacity(self.cols);
        for c in line.chars() {
            match c {
                '.' => row.push(Entity::Space),
                '#' => row.push(Entity::Asteroid),
                c => return Err(format!("Unsupported entity: {}", c)),
            }
        }
        self.entities.append(&mut row);
        Ok(())
    }

    fn asteroids_visible_quadrant(
        &self,
        rows: usize,
        origin_row: usize,
        origin_col: usize,
        mask: &mut Vec<bool>,
        row_delta_delta: isize,
        col_delta_delta: isize,
    ) -> u32 {
        let cols = self.cols;
        let mut count = 0;
        let mut row_delta: isize = 0;
        while (row_delta.abs() as usize) < rows {
            let mut col_delta: isize = 0;
            while (col_delta.abs() as usize) < cols {
                if row_delta != 0 || col_delta != 0 {
                    // println!(
                    //     "{}x{} col_delta: {}, row_delta: {}",
                    //     origin_col, origin_row, col_delta, row_delta
                    // );
                    let mut found = false;
                    let mut row = (origin_row as isize) + row_delta;
                    let mut col = (origin_col as isize) + col_delta;
                    while row >= 0 && (row as usize) < rows && col >= 0 && (col as usize) < cols {
                        // println!("{}x{} {}x{}", origin_col, origin_row, col, row);
                        let idx = (row as usize) * cols + (col as usize);
                        if !mask[idx] {
                            mask[idx] = true;
                            match self.entities[idx] {
                                Entity::Asteroid => {
                                    if !found {
                                        // println!(
                                        //     "Asteroid at {}x{} visible from {}x{}",
                                        //     col, row, origin_col, origin_row
                                        // );
                                        found = true;
                                    }
                                }
                                Entity::Space => (),
                            }
                        }
                        row += row_delta;
                        col += col_delta;
                    }
                    if found {
                        count += 1;
                    }
                }
                col_delta += col_delta_delta;
            }
            row_delta += row_delta_delta;
        }
        count
    }

    fn asteroids_visible(&self, rows: usize, origin_row: usize, origin_col: usize) -> u32 {
        let mut count = 0;
        let mut mask = vec![false; self.entities.len()];
        count += self.asteroids_visible_quadrant(rows, origin_row, origin_col, &mut mask, -1, -1);
        count += self.asteroids_visible_quadrant(rows, origin_row, origin_col, &mut mask, -1, 1);
        count += self.asteroids_visible_quadrant(rows, origin_row, origin_col, &mut mask, 1, -1);
        count += self.asteroids_visible_quadrant(rows, origin_row, origin_col, &mut mask, 1, 1);
        count
    }

    pub fn solve(&self) {
        let cols = self.cols;
        let rows = self.entities.len() / cols;
        let mut max_asteroids_visible = 0;
        for row in 0..rows {
            for col in 0..self.cols {
                match self.entities[row * cols + col] {
                    Entity::Asteroid => (),
                    Entity::Space => continue,
                }
                let asteroids_visible = self.asteroids_visible(rows, row, col);
                if asteroids_visible > max_asteroids_visible {
                    println!("{} asteroids visible at {}x{}", asteroids_visible, col, row);
                    max_asteroids_visible = asteroids_visible;
                }
            }
        }
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut lines = reader.lines();
    let mut map = match lines.next() {
        None => return Err(format!("Missing first line")),
        Some(read_line) => match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let mut map = Map::new(line.len());
                map.add_row(line)?;
                map
            }
        },
    };
    for read_line in lines {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                map.add_row(line)?;
            }
        };
    }
    map.solve();
    Ok(())
}
