use std::io::BufRead;

fn process(line: &str) -> Result<(), String> {
    let mut original_input: Vec<i32> = Vec::with_capacity(line.len());
    for i in 0..line.len() {
        let value: i32 = match line[i..i + 1].parse() {
            Err(err) => return Err(format!("Input contains non-number: {}", err)),
            Ok(v) => v,
        };
        original_input.push(value);
    }
    let message_offset: usize = line[0..7]
        .parse()
        .expect("Expected 7 digits at the start of line");

    // MAYBE I should use some sort of fancy iterator and formulas but this seems simpler
    let input_len = original_input.len() * 10000;
    if message_offset > input_len {
        println!(
            "Skipping too many inputs for input {}, skipping input",
            line
        );
        return Ok(());
    }

    original_input = {
        let mut reversed_original_input = Vec::with_capacity(original_input.len());
        for i in 0..original_input.len() {
            reversed_original_input.push(original_input[original_input.len() - i - 1]);
        }
        reversed_original_input
    };

    // we assume the input is in the 2nd half of the total input
    let calculated_length = input_len - message_offset;

    let mut values: Vec<i32> = Vec::with_capacity(calculated_length);
    for value in original_input.iter().cycle().take(calculated_length) {
        values.push(*value);
    }

    println!();
    println!("Input: {}", line);
    for phase in 1..=100 {
        let mut new_values: Vec<i32> = Vec::with_capacity(values.len());
        let mut sum: i32 = 0;
        for digit in &values {
            sum = (sum + *digit) % 10;
            new_values.push(sum);
        }
        values = new_values;
        if phase == 100 {
            print!("After phase {}: ", phase);
            for i in 1..=8 {
                print!("{}", values[values.len() - i])
            }
            println!();
        }
    }

    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                // TODO work out if this can be done without collecting
                process(line)?;
            }
        };
    }

    Ok(())
}
