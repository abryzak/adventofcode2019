use std::cmp::{max, min};
use std::collections::{HashMap, VecDeque};
use std::io::BufRead;

macro_rules! intcodedebug {
    () => {
        //        println!()
    };
    ($($arg:tt)*) => {{
        //        println!($($arg)*);
    }};
}

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<i64>, String> {
    let mut vals: Vec<i64> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

enum ParameterMode {
    PositionMode,
    ImmediateMode,
    RelativeMode,
}

fn param_mode(mode_int: u32) -> Result<ParameterMode, String> {
    let mode = match mode_int {
        0 => ParameterMode::PositionMode,
        1 => ParameterMode::ImmediateMode,
        2 => ParameterMode::RelativeMode,
        _ => return Err(format!("Unrecognized parameter mode: {}", mode_int)),
    };
    Ok(mode)
}

enum ComputationResult {
    Finished,
    RequiresInput,
    Output(i64),
}

#[derive(Clone, Debug, PartialEq)]
struct Computer {
    ip: usize,
    relative_base: usize,
    memory: Vec<i64>,
    inputs: VecDeque<i64>,
}

impl Computer {
    fn validate_address(&self, address: i64) -> Result<usize, String> {
        if address < 0 {
            return Err(format!("Out of bounds memory access: {}", address));
        }
        Ok(address as usize)
    }

    fn read_mem(&self, address: usize) -> i64 {
        let value = if address >= self.memory.len() {
            0
        } else {
            self.memory[address]
        };
        intcodedebug!("read {}: {}", address, value);
        value
    }

    fn write_mem(&mut self, address: usize, value: i64) {
        // MAYBE store separate HashMap of extended addresses
        while address >= self.memory.len() {
            self.memory.push(0);
        }
        intcodedebug!("write {} to {}", value, address);
        self.memory[address] = value;
    }

    fn next_instruction(&self) -> Result<u32, String> {
        let instruction_value: i64 = self.read_mem(self.ip);
        if instruction_value < 0 {
            return Err(format!("Invalid instruction {}", instruction_value));
        }
        Ok(instruction_value as u32)
    }

    fn read_value_param(&self, param_idx: usize, instruction: u32) -> Result<i64, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                let address = self.validate_address(raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::ImmediateMode => raw_value,
        };
        Ok(value)
    }

    fn read_address_param(&self, param_idx: usize, instruction: u32) -> Result<usize, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                // MAYBE this should never happen?
                let address = self.validate_address(raw_value)?;
                //                self.validate_address(self.read_mem(address))?
                address
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                address
            }
            ParameterMode::ImmediateMode => self.validate_address(raw_value)?,
        };
        Ok(value)
    }

    pub fn run(&mut self) -> Result<ComputationResult, String> {
        loop {
            intcodedebug!();
            let instruction = self.next_instruction()?;
            let opcode = instruction % 100;
            intcodedebug!(
                //                "IP: {}, instruction: {}, opcode: {}, rb: {}, param1: {}, param2: {}, param3: {}",
                "IP: {}, instruction: {}, opcode: {} rb: {}",
                self.ip,
                instruction,
                opcode,
                self.relative_base,
                //                self.read_mem(self.ip + 1),
                //                self.read_mem(self.ip + 2),
                //                self.read_mem(self.ip + 3)
            );
            match opcode {
                1 => {
                    // add 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 + val2);
                    self.ip += 4;
                }
                2 => {
                    // mul 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 * val2);
                    self.ip += 4;
                }
                3 => {
                    // read input and store at address 1
                    let address = self.read_address_param(1, instruction)?;
                    intcodedebug!(
                        "address: {}, {}, relative base: {}",
                        address,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    let val = match self.inputs.pop_front() {
                        None => break Ok(ComputationResult::RequiresInput),
                        Some(v) => v,
                    };
                    self.write_mem(address, val);
                    self.ip += 2;
                }
                4 => {
                    // output 1
                    let val1 = self.read_value_param(1, instruction)?;
                    self.ip += 2;
                    break Ok(ComputationResult::Output(val1));
                }
                5 => {
                    // if 1 != 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 != 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                6 => {
                    // if 1 == 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 == 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                7 => {
                    // if 1 < 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 < val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                8 => {
                    // if 1 = 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 == val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                9 => {
                    // set relative base to relative base + 1
                    let val1 = self.read_value_param(1, instruction)?;
                    intcodedebug!(
                        "set relative base, instruction: {}, val: {}, raw_param: {}, relative base: {}",
                        instruction,
                        val1,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    if val1 >= 0 {
                        self.relative_base += val1 as usize;
                    } else {
                        self.relative_base -= -val1 as usize;
                    }
                    self.ip += 2;
                }
                99 => break Ok(ComputationResult::Finished),
                _ => {
                    break Err(format!(
                        "Unexpected opcode {} at address {}",
                        opcode, self.ip
                    ))
                }
            };
        }
    }
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

enum Action {
    Paint,
    TurnAndMove,
}

enum Color {
    Black,
    White,
}

struct HullPaintingRobot {
    computer: Computer,
    x: i32,
    y: i32,
    direction: Direction,
    next_action: Action,
    painted_panels: HashMap<(i32, i32), Color>,
}

const BLACK: &str = " ";
const WHITE: &str = "█";

impl HullPaintingRobot {
    pub fn new(computer: Computer) -> HullPaintingRobot {
        let mut robot = HullPaintingRobot {
            computer,
            x: 0,
            y: 0,
            direction: Direction::Up,
            next_action: Action::Paint,
            painted_panels: HashMap::new(),
        };
        robot.painted_panels.insert((0, 0), Color::White);
        robot
    }

    fn current_color(&self) -> i64 {
        match self.painted_panels.get(&(self.x, self.y)) {
            None => 0,
            Some(Color::Black) => 0,
            Some(Color::White) => 1,
        }
    }

    fn process_paint(&mut self, value: i64) -> Result<(), String> {
        let color = match value {
            0 => Color::Black,
            1 => Color::White,
            _ => return Err(format!("Invalid color: {}", value)),
        };
        self.painted_panels.insert((self.x, self.y), color);
        Ok(())
    }

    fn process_turn_and_move(&mut self, value: i64) -> Result<(), String> {
        self.direction = match (&self.direction, value) {
            (Direction::Up, 0) => Direction::Left,
            (Direction::Up, 1) => Direction::Right,
            (Direction::Down, 0) => Direction::Right,
            (Direction::Down, 1) => Direction::Left,
            (Direction::Left, 0) => Direction::Down,
            (Direction::Left, 1) => Direction::Up,
            (Direction::Right, 0) => Direction::Up,
            (Direction::Right, 1) => Direction::Down,
            _ => return Err(format!("Invalid turn: {}", value)),
        };
        match &self.direction {
            Direction::Up => self.y -= 1,
            Direction::Down => self.y += 1,
            Direction::Left => self.x -= 1,
            Direction::Right => self.x += 1,
        }
        Ok(())
    }

    fn process(&mut self, value: i64) -> Result<(), String> {
        match self.next_action {
            Action::Paint => {
                self.process_paint(value)?;
                self.next_action = Action::TurnAndMove;
            }
            Action::TurnAndMove => {
                self.process_turn_and_move(value)?;
                self.next_action = Action::Paint;
            }
        }
        Ok(())
    }

    fn output_painting(&self) {
        let mut x_min = 0;
        let mut y_min = 0;
        let mut x_max = 0;
        let mut y_max = 0;
        for (x, y) in self.painted_panels.keys() {
            x_min = min(x_min, *x);
            y_min = min(y_min, *y);
            x_max = max(x_max, *x);
            y_max = max(y_max, *y);
        }
        println!(
            "X min/max: {}/{}, Y min/max: {}/{}",
            x_min, x_max, y_min, y_max
        );
        for y in y_min..=y_max {
            for x in x_min..=x_max {
                match self.painted_panels.get(&(x, y)) {
                    None => print!("{}", BLACK),
                    Some(Color::Black) => print!("{}", BLACK),
                    Some(Color::White) => print!("{}", WHITE),
                }
            }
            println!();
        }
    }

    pub fn run(&mut self) -> Result<(), String> {
        loop {
            match self.computer.run()? {
                ComputationResult::Finished => {
                    break;
                }
                ComputationResult::RequiresInput => {
                    self.computer.inputs.push_back(self.current_color());
                }
                ComputationResult::Output(val) => {
                    self.process(val)?;
                }
            }
        }
        println!("Painted {} panels", self.painted_panels.len());
        self.output_painting();
        Ok(())
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let program = line.trim();
                if program.len() == 0 || program.starts_with("#") {
                    continue;
                }
                let base_memory = split_to_numbers(program)?;
                let computer = Computer {
                    ip: 0,
                    relative_base: 0,
                    memory: base_memory,
                    inputs: VecDeque::new(),
                };
                let mut robot = HullPaintingRobot::new(computer);
                robot.run()?;
            }
        };
    }
    Ok(())
}
