use std::io::BufRead;

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<usize>, String> {
    let mut vals: Vec<usize> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

fn run(memory: &mut Vec<usize>) -> Result<(), String> {
    let mut ip = 0;
    loop {
        if ip >= memory.len() {
            break Err(format!("Unexpected end of input at index {}", ip));
        }
        let opcode = memory[ip];
        match opcode {
            1 => {
                if ip + 4 >= memory.len() {
                    return Err(format!("Out of bounds read at {}", ip));
                }
                let address1 = memory[ip + 1];
                let address2 = memory[ip + 2];
                let address3 = memory[ip + 3];
                let val1 = memory[address1];
                let val2 = memory[address2];
                memory[address3] = val1 + val2;
                ip += 4;
            }
            2 => {
                if ip + 4 >= memory.len() {
                    return Err(format!("Out of bounds read at {}", ip));
                }
                let address1 = memory[ip + 1];
                let address2 = memory[ip + 2];
                let address3 = memory[ip + 3];
                let val1 = memory[address1];
                let val2 = memory[address2];
                memory[address3] = val1 * val2;
                ip += 4;
            }
            99 => break Ok(()),
            _ => break Err(format!("Unexpected opcode {} at address {}", opcode, ip)),
        };
    }?;
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for line in reader.lines() {
        match line {
            Err(err) => Err(format!("Error reading line: {}", err)),
            Ok(ref program) => {
                let memory = split_to_numbers(program)?;
                for noun in 0..99 {
                    for verb in 0..99 {
                        let mut new_memory = memory.clone();
                        // TODO pass in corruptions
                        new_memory[1] = noun;
                        new_memory[2] = verb;
                        run(&mut new_memory)?;
                        if new_memory[0] == 19690720 {
                            println!("Noun {} verb {} = {}", noun, verb, new_memory[0]);
                            println!("Answer: {}", (100 * noun + verb))
                        }
                    }
                }
                Ok(())
            }
        }?;
    }
    Ok(())
}
