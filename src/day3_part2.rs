use std::cmp::{max, min};
use std::io::BufRead;

fn max_distance_from_port(path: &Vec<&str>) -> (i32, i32, i32, i32) {
    let mut up = 0;
    let mut down = 0;
    let mut left = 0;
    let mut right = 0;
    let mut x = 0;
    let mut y = 0;
    for segment in path {
        match segment.as_bytes()[0] as char {
            'U' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                y -= value;
                if y < up {
                    up = y;
                }
            }
            'D' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                y += value;
                if y > down {
                    down = y;
                }
            }
            'L' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                x -= value;
                if x < left {
                    left = x;
                }
            }
            'R' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                x += value;
                if x > right {
                    right = x;
                }
            }
            _ => panic!("Unrecognized segment {}", segment),
        }
    }
    return (up, down, left, right);
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut lines = reader.lines();
    let wire1 = lines.next().unwrap().expect("expected wire1");
    let wire2 = lines.next().unwrap().expect("expected wire2");

    let wire1_path: Vec<&str> = wire1.split(',').collect();
    let (w1u, w1d, w1l, w1r) = max_distance_from_port(&wire1_path);
    println!(
        "Wire 1 extremities. up: {}, down: {}, left: {}, right: {}",
        w1u, w1d, w1l, w1r
    );

    let wire2_path: Vec<&str> = wire2.split(',').collect();
    let (w2u, w2d, w2l, w2r) = max_distance_from_port(&wire2_path);
    println!(
        "Wire 2 extremities. up: {}, down: {}, left: {}, right: {}",
        w2u, w2d, w2l, w2r
    );

    // give a bit of a buffer because I'm lazy
    let width: usize = (max(w1r, w2r) - min(w1l, w2l) + 10) as usize;
    let height: usize = (max(w1d, w2d) - min(w1u, w2u) + 10) as usize;
    println!("Total width: {}, height: {}", width, height);

    let start_x: usize = width - max(w1r as usize, w2r as usize) - 5;
    let start_y = height - max(w1d as usize, w2d as usize) - 5;
    println!("Start x: {}, y: {}", start_x, start_y);

    let w1_cells = cells(&wire1_path, width, height, start_x, start_y);
    let w2_cells = cells(&wire2_path, width, height, start_x, start_y);
    let mut lowest_steps: i32 = (width * height) as i32;
    let mut lowest_x = start_x;
    let mut lowest_y = start_y;
    for x in 0..width {
        for y in 0..height {
            let idx = y * width + x;
            if w1_cells[idx] > 0 && w2_cells[idx] > 0 {
                let steps = w1_cells[idx] + w2_cells[idx];
                println!("Intersection at x: {}, y: {}, steps: {}", x, y, steps);
                if steps < lowest_steps {
                    lowest_steps = steps;
                    lowest_x = x;
                    lowest_y = y;
                }
            }
        }
    }
    println!(
        "Lowest steps: {} at x: {}, y: {}",
        lowest_steps, lowest_x, lowest_y
    );

    Ok(())
}

fn cells(
    path: &Vec<&str>,
    width: usize,
    height: usize,
    start_x: usize,
    start_y: usize,
) -> Vec<i32> {
    let mut cells = vec![0; width * height];
    let mut idx = start_y * width + start_x;
    let mut current_distance = 0;
    for segment in path {
        match segment.as_bytes()[0] as char {
            'U' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                for _i in 0..value {
                    current_distance += 1;
                    idx -= width;
                    if cells[idx] == 0 {
                        cells[idx] = current_distance;
                    }
                }
            }
            'D' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                for _i in 0..value {
                    current_distance += 1;
                    idx += width;
                    if cells[idx] == 0 {
                        cells[idx] = current_distance;
                    }
                }
            }
            'L' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                for _i in 0..value {
                    current_distance += 1;
                    idx += 1;
                    if cells[idx] == 0 {
                        cells[idx] = current_distance;
                    }
                }
            }
            'R' => {
                let value: i32 = segment[1..]
                    .parse()
                    .expect(format!("Unable to parse {}", segment).as_str());
                for _i in 0..value {
                    current_distance += 1;
                    idx -= 1;
                    if cells[idx] == 0 {
                        cells[idx] = current_distance;
                    }
                }
            }
            _ => panic!("Unrecognized segment {}", segment),
        }
    }
    cells
}
