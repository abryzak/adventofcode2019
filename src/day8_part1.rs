use std::io::BufRead;

fn count(s: &str, c: char) -> u32 {
    let mut count = 0;
    for s_char in s.chars() {
        if s_char == c {
            count += 1;
        }
    }
    count
}

fn solve_puzzle(line: &str, width: usize, height: usize) -> Result<(), String> {
    let mut layers: Vec<&str> = Vec::new();
    let mut i = 0;
    while i < line.len() {
        let end = i + (width * height);
        if end > line.len() {
            return Err(format!("Incomplete layer"));
        }
        layers.push(&line[i..end]);
        i = end;
    }
    let mut min_zeroes = u32::max_value();
    let mut solution = 0;
    for layer in layers {
        let zero_count = count(layer, '0');
        if zero_count < min_zeroes {
            min_zeroes = zero_count;
            solution = count(layer, '1') * count(layer, '2');
        }
    }
    println!("{}", solution);
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let trimmed = line.trim();
                if trimmed.len() == 0 || trimmed.starts_with("#") {
                    continue;
                }
                solve_puzzle(trimmed, 25, 6)?;
            }
        };
    }
    Ok(())
}
