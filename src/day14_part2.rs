use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct ElementWithAmount {
    element: String,
    amount: u64,
}

impl ToString for ElementWithAmount {
    fn to_string(&self) -> String {
        format!("{} {}", self.amount, self.element)
    }
}

impl ElementWithAmount {
    pub fn new(element_with_amount: &str) -> Result<Self, String> {
        let parts: Vec<&str> = element_with_amount.trim().splitn(2, " ").collect();
        if parts.len() != 2 {
            return Err(format!(
                "Expected both an element and an amount: {}",
                element_with_amount
            ));
        }
        let amount: u64 = match parts[0].parse() {
            Err(err) => {
                return Err(format!(
                    "Unable to parse amount from {}: {}",
                    element_with_amount, err
                ))
            }
            Ok(v) => v,
        };
        let element = parts[1];
        return Ok(ElementWithAmount {
            element: String::from(element),
            amount,
        });
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct Reaction {
    output: ElementWithAmount,
    inputs: Vec<ElementWithAmount>,
}

impl Reaction {
    fn required_inputs_for_amount(&self, amount_required: u64) -> (u64, Vec<ElementWithAmount>) {
        let mut times = amount_required / self.output.amount;
        let mut actually_produced = times * self.output.amount;
        if times * self.output.amount < amount_required {
            times += 1;
            actually_produced += self.output.amount;
        }
        let mut required_inputs = self.inputs.clone();
        for input in required_inputs.iter_mut() {
            input.amount *= times;
        }
        (actually_produced, required_inputs)
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut reaction_by_output: HashMap<String, Reaction> = HashMap::new();
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                // TODO work out if this can be done without collecting
                let parts: Vec<&str> = line.splitn(2, "=>").collect();
                if parts.len() != 2 {
                    return Err(format!("Expected inputs & output separated by =>"));
                };
                let output = ElementWithAmount::new(parts[1])?;
                let inputs: Vec<ElementWithAmount> = parts[0]
                    .split(",")
                    .map(|input_str| ElementWithAmount::new(input_str))
                    .collect::<Result<Vec<ElementWithAmount>, String>>()?;
                reaction_by_output.insert(output.element.clone(), Reaction { output, inputs })
            }
        };
    }

    let mut ore_required: u64 = 0;
    let mut remaining_outputs_by_element: HashMap<String, u64> = HashMap::new();
    let mut extra_produced_by_element: HashMap<String, u64> = HashMap::new();
    // a smarter way is probably to work out the ratios with a float and calculate it that way
    // but I just estimated and refined the estimation until it was correct
    let fuel_to_produce = 1993284;
    remaining_outputs_by_element.insert(String::from("FUEL"), fuel_to_produce);
    while remaining_outputs_by_element.len() > 0 {
        // TODO work out if I can do this without cloning
        let first_key = remaining_outputs_by_element.keys().next().unwrap().clone();
        let (element, mut required_amount) = remaining_outputs_by_element
            .remove_entry(&first_key)
            .unwrap();
        match extra_produced_by_element.get_mut(&element) {
            None => {}
            Some(extra_produced) if *extra_produced > required_amount => {
                *extra_produced -= required_amount;
                required_amount = 0;
            }
            Some(extra_produced) => {
                required_amount -= *extra_produced;
                extra_produced_by_element.remove(&element);
            }
        }
        if required_amount > 0 {
            let reaction = match reaction_by_output.get(&element) {
                None => {
                    return Err(format!(
                        "We require {} but found no reaction that produces it",
                        element
                    ))
                }
                Some(v) => v,
            };
            let (amount_produced, inputs_needed) =
                reaction.required_inputs_for_amount(required_amount);
            assert!(amount_produced >= required_amount);
            let extra_produced = amount_produced - required_amount;
            if extra_produced > 0 {
                extra_produced_by_element
                    .entry(element.clone())
                    .and_modify(|e| *e += extra_produced)
                    .or_insert(extra_produced);
            }
            for input in inputs_needed {
                if input.element.as_str().eq("ORE") {
                    ore_required += input.amount;
                    continue;
                }
                remaining_outputs_by_element
                    .entry(input.element.clone())
                    .and_modify(|v| *v += input.amount)
                    .or_insert(input.amount);
            }
        }
        // TODO work out how to call this without having to clone the element
        remaining_outputs_by_element.remove(&element.clone());
    }
    println!("{} ore produces {} fuel", ore_required, fuel_to_produce);

    Ok(())
}
