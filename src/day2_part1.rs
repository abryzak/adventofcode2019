use std::io::BufRead;

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<usize>, String> {
    let mut vals: Vec<usize> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

fn run(program: &String) -> Result<(), String> {
    // TODO check bounds
    println!("Executing program {}", program);
    let mut memory = split_to_numbers(program)?;
    // TODO pass in corruptions
    memory[1] = 12;
    memory[2] = 2;
    let mut ip = 0;
    loop {
        if ip >= memory.len() {
            break Err(format!("Unexpected end of input at index {}", ip));
        }
        let opcode = memory[ip];
        println!("Executing {}: {}", ip, opcode);
        match opcode {
            1 => {
                if ip + 4 >= memory.len() {
                    return Err(format!("Out of bounds read at {}", ip));
                }
                let address1 = memory[ip + 1];
                let address2 = memory[ip + 2];
                let address3 = memory[ip + 3];
                let val1 = memory[address1];
                let val2 = memory[address2];
                memory[address3] = val1 + val2;
                println!("Add {} and {}, output to {}", val1, val2, address3);
                ip += 4;
            }
            2 => {
                if ip + 4 >= memory.len() {
                    return Err(format!("Out of bounds read at {}", ip));
                }
                let address1 = memory[ip + 1];
                let address2 = memory[ip + 2];
                let address3 = memory[ip + 3];
                let val1 = memory[address1];
                let val2 = memory[address2];
                memory[address3] = val1 * val2;
                println!("Mul {} and {}, output to {}", val1, val2, address3);
                ip += 4;
            }
            99 => break Ok(()),
            _ => break Err(format!("Unexpected opcode {} at address {}", opcode, ip)),
        };
    }?;
    let answer = memory[0];
    println!("Answer: {}", answer);
    println!();
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for line in reader.lines() {
        match line {
            Err(err) => Err(format!("Error reading line: {}", err)),
            Ok(ref program) => run(program),
        }?;
    }
    Ok(())
}
