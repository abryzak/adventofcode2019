use std::io::BufRead;

fn _mass_to_fuel_recursive(mass: i32) -> i32 {
    match mass / 3 - 2 {
        fuel if fuel <= 0 => 0,
        fuel => fuel + _mass_to_fuel_recursive(fuel),
    }
}

fn _mass_to_fuel_iterative(mass: i32) -> i32 {
    let mut total_fuel: i32 = 0;
    let mut remaining_mass = mass;
    loop {
        let fuel = remaining_mass / 3 - 2;
        if fuel <= 0 {
            break;
        }
        total_fuel += fuel;
        remaining_mass = fuel;
    }
    total_fuel
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let total_fuel: i32 = reader.lines().try_fold(0, |total_fuel, line| match line {
        Err(err) => Err(format!("Error reading line: {}", err)),
        Ok(ref s) if s.is_empty() => Ok(total_fuel),
        Ok(s) => match s.parse() {
            Ok(mass) => Ok(total_fuel + _mass_to_fuel_iterative(mass)),
            Err(err) => Err(format!(
                "Error parsing integer from line \"{}\": {}",
                s, err
            )),
        },
    })?;

    println!("Fuel required: {}", total_fuel);
    Ok(())
}
