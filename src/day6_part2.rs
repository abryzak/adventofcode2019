use std::collections::{HashMap, HashSet};
use std::io::BufRead;

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut you_orbiting: String = String::new();
    let mut san_orbiting: String = String::new();
    let mut orbiters_by_orbitee: HashMap<String, HashSet<String>> = HashMap::new();
    for line in reader.lines() {
        match line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(orbit) => {
                let orbit_parts: Vec<&str> = orbit.splitn(3, ")").collect();
                if orbit_parts.len() != 2 {
                    return Err(format!("Invalid orbit: {}", orbit));
                }
                let orbitee = String::from(orbit_parts[0]);
                let orbiter = String::from(orbit_parts[1]);
                if orbiter.as_str() == "YOU" {
                    you_orbiting = orbitee.clone();
                } else if orbiter.as_str() == "SAN" {
                    san_orbiting = orbitee.clone();
                }
                let orbiters = orbiters_by_orbitee.entry(orbitee).or_insert(HashSet::new());
                orbiters.insert(orbiter);
            }
        }
    }

    if you_orbiting.len() == 0 {
        return Err(String::from("YOU not found"));
    } else if san_orbiting.len() == 0 {
        return Err(String::from("SAN not found"));
    }

    println!("YOU are orbiting {}", you_orbiting);
    println!("SAN is orbiting {}", san_orbiting);

    let mut orbitee_by_orbiter: HashMap<String, String> = HashMap::new();
    for entry in orbiters_by_orbitee {
        let orbitee = entry.0;
        for orbiter in entry.1 {
            match orbitee_by_orbiter.insert(orbiter.clone(), orbitee.clone()) {
                None => {}
                Some(existing_orbitee) => {
                    return Err(format!(
                        "{} can't orbit {}, already orbiting {}",
                        orbiter, orbitee, existing_orbitee
                    ));
                }
            }
        }
    }

    let mut orbital_transfers_to_reach_for_you: HashMap<String, i32> = HashMap::new();
    let mut current_orbiter = &you_orbiting;
    let mut distance = 0;
    loop {
        orbital_transfers_to_reach_for_you.insert(current_orbiter.clone(), distance);
        current_orbiter = match orbitee_by_orbiter.get(current_orbiter) {
            None => break,
            Some(v) => v,
        };
        distance += 1;
    }

    distance = 0;
    current_orbiter = &san_orbiting;
    let orbit_transfers = loop {
        match orbital_transfers_to_reach_for_you.get(current_orbiter) {
            Some(v) => {
                println!("Common ancestor {}", current_orbiter);
                break distance + *v;
            }
            None => {}
        }
        current_orbiter = match orbitee_by_orbiter.get(current_orbiter) {
            None => return Err(String::from("No common ancestor for YOU and SAN")),
            Some(v) => v,
        };
        distance += 1;
    };
    println!("Orbit transfer: {}", orbit_transfers);

    Ok(())
}
