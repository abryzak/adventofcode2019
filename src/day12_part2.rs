use num_integer::lcm;
use regex::Regex;
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct Moon {
    x_pos: i32,
    y_pos: i32,
    z_pos: i32,
    x_velocity: i32,
    y_velocity: i32,
    z_velocity: i32,
}

impl Moon {
    pub fn new(x_pos: i32, y_pos: i32, z_pos: i32) -> Moon {
        return Moon {
            x_pos,
            y_pos,
            z_pos,
            x_velocity: 0,
            y_velocity: 0,
            z_velocity: 0,
        };
    }

    fn adjust_velocity(&mut self, other: &Moon) {
        if self.x_pos < other.x_pos {
            self.x_velocity += 1;
        } else if self.x_pos > other.x_pos {
            self.x_velocity -= 1;
        }
        if self.y_pos < other.y_pos {
            self.y_velocity += 1;
        } else if self.y_pos > other.y_pos {
            self.y_velocity -= 1;
        }
        if self.z_pos < other.z_pos {
            self.z_velocity += 1;
        } else if self.z_pos > other.z_pos {
            self.z_velocity -= 1;
        }
    }

    fn step(&mut self) {
        self.x_pos += self.x_velocity;
        self.y_pos += self.y_velocity;
        self.z_pos += self.z_velocity;
    }
}

fn solve_moons(moons: &mut Vec<Moon>) -> Result<(), String> {
    let mut existing_x = -1;
    let mut existing_y = -1;
    let mut existing_z = -1;
    let mut previous_x: HashMap<Vec<(i32, i32)>, i32> = HashMap::new();
    let mut previous_y: HashMap<Vec<(i32, i32)>, i32> = HashMap::new();
    let mut previous_z: HashMap<Vec<(i32, i32)>, i32> = HashMap::new();
    for step in 0..1000000000 {
        for i in 1..moons.len() {
            let (left, right) = moons.split_at_mut(i);
            let left_moon = left.get_mut(left.len() - 1).unwrap();
            for right_moon in right.iter_mut() {
                left_moon.adjust_velocity(right_moon);
                right_moon.adjust_velocity(left_moon);
            }
        }

        for moon in moons.iter_mut() {
            moon.step();
        }

        if existing_x == -1 {
            let x_key: Vec<(i32, i32)> = moons
                .iter()
                .map(|moon| (moon.x_pos, moon.x_velocity))
                .collect();
            let new_x = previous_x.entry(x_key).or_insert(step);
            if *new_x != step {
                existing_x = step;
                println!(
                    "Found duplicate X state at step {}, previous state was at step {}",
                    step, new_x
                );
            }
        }

        if existing_y == -1 {
            let y_key: Vec<(i32, i32)> = moons
                .iter()
                .map(|moon| (moon.y_pos, moon.y_velocity))
                .collect();
            let new_y = previous_y.entry(y_key).or_insert(step);
            if *new_y != step {
                existing_y = step;
                println!(
                    "Found duplicate y state at step {}, previous state was at step {}",
                    step, new_y
                );
            }
        }

        if existing_z == -1 {
            let z_key: Vec<(i32, i32)> = moons
                .iter()
                .map(|moon| (moon.z_pos, moon.z_velocity))
                .collect();
            let new_z = previous_z.entry(z_key).or_insert(step);
            if *new_z != step {
                existing_z = step;
                println!(
                    "Found duplicate z state at step {}, previous state was at step {}",
                    step, new_z
                );
            }
        }

        if existing_x >= 0 && existing_y >= 0 && existing_z >= 0 {
            break;
        }
    }

    println!(
        "LCM of xyz is {}",
        lcm(lcm(existing_x as i64, existing_y as i64), existing_z as i64)
    );

    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let re = Regex::new(r"^<x=(-?[1-9]\d*|0),\s*y=(-?[1-9]\d*|0),\s*z=(-?[1-9]\d*|0)>$").unwrap();
    let mut moons: Vec<Moon> = Vec::new();
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => match re.captures(line) {
                None => return Err(format!("Invalid moon definition: {}", line)),
                Some(c) => {
                    // technically I should handle potential overflows here more gracefully
                    let x_pos: i32 = c.get(1).unwrap().as_str().parse().unwrap();
                    let y_pos: i32 = c.get(2).unwrap().as_str().parse().unwrap();
                    let z_pos: i32 = c.get(3).unwrap().as_str().parse().unwrap();
                    moons.push(Moon::new(x_pos, y_pos, z_pos));
                }
            },
        };
    }
    solve_moons(&mut moons)
}
