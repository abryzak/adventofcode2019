use regex::Regex;
use std::io::BufRead;

struct Moon {
    x_pos: i32,
    y_pos: i32,
    z_pos: i32,
    x_velocity: i32,
    y_velocity: i32,
    z_velocity: i32,
}

impl Moon {
    pub fn new(x_pos: i32, y_pos: i32, z_pos: i32) -> Moon {
        return Moon {
            x_pos,
            y_pos,
            z_pos,
            x_velocity: 0,
            y_velocity: 0,
            z_velocity: 0,
        };
    }

    fn adjust_velocity(&mut self, other: &Moon) {
        if self.x_pos < other.x_pos {
            self.x_velocity += 1;
        } else if self.x_pos > other.x_pos {
            self.x_velocity -= 1;
        }
        if self.y_pos < other.y_pos {
            self.y_velocity += 1;
        } else if self.y_pos > other.y_pos {
            self.y_velocity -= 1;
        }
        if self.z_pos < other.z_pos {
            self.z_velocity += 1;
        } else if self.z_pos > other.z_pos {
            self.z_velocity -= 1;
        }
    }

    fn step(&mut self) {
        self.x_pos += self.x_velocity;
        self.y_pos += self.y_velocity;
        self.z_pos += self.z_velocity;
    }

    fn potential_energy(&self) -> i32 {
        self.x_pos.abs() + self.y_pos.abs() + self.z_pos.abs()
    }

    fn kinetic_energy(&self) -> i32 {
        self.x_velocity.abs() + self.y_velocity.abs() + self.z_velocity.abs()
    }

    fn total_energy(&self) -> i32 {
        self.potential_energy() * self.kinetic_energy()
    }
}

fn solve_moons(moons: &mut Vec<Moon>) -> Result<(), String> {
    for _step in 0..1000 {
        for i in 1..moons.len() {
            let (left, right) = moons.split_at_mut(i);
            let left_moon = left.get_mut(left.len() - 1).unwrap();
            for right_moon in right.iter_mut() {
                left_moon.adjust_velocity(right_moon);
                right_moon.adjust_velocity(left_moon);
            }
        }

        for moon in moons.iter_mut() {
            moon.step();
        }
    }
    let mut total_energy = 0;
    for moon in moons {
        total_energy += moon.total_energy();
    }
    println!("Total energy after 1000 steps: {}", total_energy);
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let re = Regex::new(r"^<x=(-?[1-9]\d*|0),\s*y=(-?[1-9]\d*|0),\s*z=(-?[1-9]\d*|0)>$").unwrap();
    let mut moons: Vec<Moon> = Vec::new();
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => match re.captures(line) {
                None => return Err(format!("Invalid moon definition: {}", line)),
                Some(c) => {
                    // technically I should handle potential overflows here more gracefully
                    let x_pos: i32 = c.get(1).unwrap().as_str().parse().unwrap();
                    let y_pos: i32 = c.get(2).unwrap().as_str().parse().unwrap();
                    let z_pos: i32 = c.get(3).unwrap().as_str().parse().unwrap();
                    moons.push(Moon::new(x_pos, y_pos, z_pos));
                }
            },
        };
    }
    solve_moons(&mut moons)
}
