use std::cmp::max;
use std::collections::VecDeque;
use std::io::BufRead;

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<i32>, String> {
    let mut vals: Vec<i32> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

enum ParameterMode {
    PositionMode,
    ImmediateMode,
}

fn param_mode(mode_int: u32) -> Result<ParameterMode, String> {
    let mode = match mode_int {
        0 => ParameterMode::PositionMode,
        1 => ParameterMode::ImmediateMode,
        _ => return Err(format!("Unrecognized parameter mode: {}", mode_int)),
    };
    Ok(mode)
}

enum ComputationResult {
    Finished,
    RequiresInput,
    Output(i32),
}

#[derive(Clone, Debug, PartialEq)]
struct Computer {
    ip: usize,
    memory: Vec<i32>,
    inputs: VecDeque<i32>,
}

impl Computer {
    fn validate_ip(&self) -> Result<usize, String> {
        let ip = self.ip;
        if ip >= self.memory.len() {
            return Err(format!("Unexpected end of input at index {}", ip));
        }
        Ok(ip)
    }

    fn validate_address(&self, address: i32) -> Result<usize, String> {
        if address < 0 || (address as usize) >= self.memory.len() {
            return Err(format!("Out of bounds memory access: {}", address));
        }
        Ok(address as usize)
    }

    fn validate_address_u(&self, address: usize) -> Result<usize, String> {
        if address >= self.memory.len() {
            return Err(format!("Out of bounds memory access: {}", address));
        }
        Ok(address as usize)
    }

    fn next_instruction(&self) -> Result<u32, String> {
        let instruction_value: i32 = self.memory[self.validate_ip()?];
        if instruction_value < 0 {
            return Err(format!("Invalid instruction {}", instruction_value));
        }
        Ok(instruction_value as u32)
    }

    fn read_value_param(&self, param_idx: usize, instruction: u32) -> Result<i32, String> {
        let raw_value = self.memory[self.ip + param_idx];
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                let address = self.validate_address(raw_value)?;
                self.memory[address]
            }
            ParameterMode::ImmediateMode => raw_value,
        };
        Ok(value)
    }

    fn read_address_param(&self, param_idx: usize, instruction: u32) -> Result<usize, String> {
        let address = self.read_value_param(param_idx, instruction)?;
        self.validate_address(address)
    }

    fn read_address(&self, param_idx: usize) -> Result<usize, String> {
        let raw_value = self.memory[self.ip + param_idx];
        self.validate_address(raw_value)
    }

    pub fn run(&mut self) -> Result<ComputationResult, String> {
        loop {
            let instruction = self.next_instruction()?;
            let opcode = instruction % 100;
            match opcode {
                1 => {
                    // add 1, 2, store at address 3
                    self.validate_address_u(self.ip + 4)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address(3)?;
                    self.memory[address] = val1 + val2;
                    self.ip += 4;
                }
                2 => {
                    // mul 1, 2, store at address 3
                    self.validate_address_u(self.ip + 4)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address(3)?;
                    self.memory[address] = val1 * val2;
                    self.ip += 4;
                }
                3 => {
                    // read input and store at address 1
                    self.validate_address_u(self.ip + 2)?;
                    let address = self.read_address(1)?;
                    let val = match self.inputs.pop_front() {
                        None => break Ok(ComputationResult::RequiresInput),
                        Some(v) => v,
                    };
                    self.memory[address] = val;
                    self.ip += 2;
                }
                4 => {
                    // output 1
                    self.validate_address_u(self.ip + 2)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    self.ip += 2;
                    break Ok(ComputationResult::Output(val1));
                }
                5 => {
                    // if 1 != 0, jump to address 2
                    self.validate_address_u(self.ip + 3)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let address = self.read_address_param(2, instruction)?;
                    if val1 != 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                6 => {
                    // if 1 == 0, jump to address 2
                    self.validate_address_u(self.ip + 3)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let address = self.read_address(2)?;
                    if val1 == 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                7 => {
                    self.validate_address_u(self.ip + 4)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address(3)?;
                    self.memory[address] = if val1 < val2 { 1 } else { 0 };
                    self.ip += 4;
                }
                8 => {
                    self.validate_address_u(self.ip + 4)?;
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address(3)?;
                    self.memory[address] = if val1 == val2 { 1 } else { 0 };
                    self.ip += 4;
                }
                99 => break Ok(ComputationResult::Finished),
                _ => {
                    break Err(format!(
                        "Unexpected opcode {} at address {}",
                        opcode, self.ip
                    ))
                }
            };
        }
    }
}

fn run_amps(base_memory: &Vec<i32>, phases: &[i32; 5]) -> Result<i32, String> {
    let mut amp_computers = [
        Computer {
            ip: 0,
            memory: base_memory.clone(),
            inputs: VecDeque::new(),
        },
        Computer {
            ip: 0,
            memory: base_memory.clone(),
            inputs: VecDeque::new(),
        },
        Computer {
            ip: 0,
            memory: base_memory.clone(),
            inputs: VecDeque::new(),
        },
        Computer {
            ip: 0,
            memory: base_memory.clone(),
            inputs: VecDeque::new(),
        },
        Computer {
            ip: 0,
            memory: base_memory.clone(),
            inputs: VecDeque::new(),
        },
    ];

    for i in 0..5 {
        amp_computers[i].inputs.push_back(phases[i]);
    }
    amp_computers[0].inputs.push_back(0);

    let mut last_output = 0;
    'outer: loop {
        for i in 0..4 {
            loop {
                match amp_computers[i].run()? {
                    ComputationResult::Finished => {
                        break;
                    }
                    ComputationResult::RequiresInput => {
                        break;
                    }
                    ComputationResult::Output(val) => {
                        amp_computers[i + 1].inputs.push_back(val);
                    }
                }
            }
        }
        loop {
            match amp_computers[4].run()? {
                ComputationResult::Finished => {
                    break 'outer;
                }
                ComputationResult::RequiresInput => {
                    break;
                }
                ComputationResult::Output(val) => {
                    last_output = val;
                    amp_computers[0].inputs.push_back(val);
                }
            }
        }
    }
    Ok(last_output)
}

// Heap's algorithm for generating permutations
// https://en.wikipedia.org/wiki/Heap%27s_algorithm
fn solve_permutation(
    base_memory: &Vec<i32>,
    k: usize,
    phases: &mut [i32; 5],
) -> Result<i32, String> {
    if k == 1 {
        let result = run_amps(base_memory, phases)?;
        return Ok(result);
    }
    let mut result = solve_permutation(base_memory, k - 1, phases)?;
    for i in 0..k - 1 {
        if k & 1 == 0 {
            let tmp = phases[i];
            phases[i] = phases[k - 1];
            phases[k - 1] = tmp;
        } else {
            let tmp = phases[0];
            phases[0] = phases[k - 1];
            phases[k - 1] = tmp;
        }
        result = max(result, solve_permutation(base_memory, k - 1, phases)?);
    }
    Ok(result)
}

fn solve_all_permutations(base_memory: &Vec<i32>) -> Result<i32, String> {
    let mut phases = [5, 6, 7, 8, 9];
    solve_permutation(base_memory, 5, &mut phases)
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let program = line.trim();
                if program.len() == 0 || program.starts_with("#") {
                    continue;
                }
                let base_memory = split_to_numbers(program)?;
                let result = solve_all_permutations(&base_memory)?;
                println!();
                println!("Highest Signal: {}", result);
            }
        };
    }
    Ok(())
}
