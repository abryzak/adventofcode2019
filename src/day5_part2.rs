use std::io::{stdin, BufRead};

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<i32>, String> {
    let mut vals: Vec<i32> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

enum ParameterMode {
    PositionMode,
    ImmediateMode,
}

fn param_mode(mode_int: u32) -> Result<ParameterMode, String> {
    let mode = match mode_int {
        0 => ParameterMode::PositionMode,
        1 => ParameterMode::ImmediateMode,
        _ => return Err(format!("Unrecognized parameter mode: {}", mode_int)),
    };
    Ok(mode)
}

fn validate_address(address: i32, memory: &Vec<i32>) -> Result<usize, String> {
    if address < 0 || (address as usize) >= memory.len() {
        return Err(format!("Out of bounds memory access: {}", address));
    }
    Ok(address as usize)
}

fn validate_address_u(address: usize, memory: &Vec<i32>) -> Result<usize, String> {
    if address >= memory.len() {
        return Err(format!("Out of bounds memory access: {}", address));
    }
    Ok(address as usize)
}

fn param_value(param: i32, mode: ParameterMode, memory: &Vec<i32>) -> Result<i32, String> {
    let value = match mode {
        ParameterMode::PositionMode => {
            let address = validate_address(param, memory)?;
            memory[address]
        }
        ParameterMode::ImmediateMode => param,
    };
    Ok(value)
}

// TODO provide generic input & output, not just stdin/stdout

fn run(memory: &mut Vec<i32>) -> Result<(), String> {
    let mut ip: usize = 0;
    loop {
        if ip >= memory.len() {
            break Err(format!("Unexpected end of input at index {}", ip));
        }
        let instruction_value: i32 = memory[ip];
        if instruction_value < 0 {
            return Err(format!("Invalid instruction {}", instruction_value));
        }
        let instruction = instruction_value as u32;
        let opcode = instruction % 100;
        let param1_mode = param_mode((instruction / 100) % 10)?;
        let param2_mode = param_mode((instruction / 1000) % 10)?;
        let _param3_mode = param_mode((instruction / 10000) % 10)?;
        match opcode {
            1 => {
                validate_address_u(ip + 4, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let param3 = memory[ip + 3];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                let address = validate_address(param3, memory)?;
                memory[address] = val1 + val2;
                ip += 4;
            }
            2 => {
                validate_address_u(ip + 4, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let param3 = memory[ip + 3];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                let address = validate_address(param3, memory)?;
                memory[address] = val1 * val2;
                ip += 4;
            }
            3 => {
                validate_address_u(ip + 2, memory)?;
                let param1 = memory[ip + 1];
                let mut read_line = String::new();
                match stdin().read_line(&mut read_line) {
                    Err(err) => return Err(format!("Error reading input: {}", err)),
                    _ => {}
                };
                let input = read_line.trim_end();
                let val: i32 = match input.parse() {
                    Ok(v) => v,
                    Err(err) => {
                        return Err(format!(
                            "Expected numeric input but got \"{}\": {}",
                            input, err
                        ))
                    }
                };
                let address = validate_address(param1, memory)?;
                memory[address] = val;
                ip += 2;
            }
            4 => {
                validate_address_u(ip + 2, memory)?;
                let param1 = memory[ip + 1];
                let val1 = param_value(param1, param1_mode, memory)?;
                println!("{}", val1);
                ip += 2;
            }
            5 => {
                validate_address_u(ip + 3, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                if val1 != 0 {
                    ip = validate_address(val2, memory)?;
                } else {
                    ip += 3;
                }
            }
            6 => {
                validate_address_u(ip + 3, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                if val1 == 0 {
                    ip = validate_address(val2, memory)?;
                } else {
                    ip += 3;
                }
            }
            7 => {
                validate_address_u(ip + 4, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let param3 = memory[ip + 3];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                let address = validate_address(param3, memory)?;
                memory[address] = if val1 < val2 { 1 } else { 0 };
                ip += 4;
            }
            8 => {
                validate_address_u(ip + 4, memory)?;
                let param1 = memory[ip + 1];
                let param2 = memory[ip + 2];
                let param3 = memory[ip + 3];
                let val1 = param_value(param1, param1_mode, memory)?;
                let val2 = param_value(param2, param2_mode, memory)?;
                let address = validate_address(param3, memory)?;
                memory[address] = if val1 == val2 { 1 } else { 0 };
                ip += 4;
            }
            99 => break Ok(()),
            _ => break Err(format!("Unexpected opcode {} at address {}", opcode, ip)),
        };
    }?;
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for line in reader.lines() {
        match line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref program) => {
                let mut memory = split_to_numbers(program)?;
                run(&mut memory)?;
            }
        };
    }
    Ok(())
}
