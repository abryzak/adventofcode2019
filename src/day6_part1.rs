use std::collections::{HashMap, HashSet};
use std::io::BufRead;

fn count_total_orbits(
    body: &String,
    orbiters_by_orbitee: &HashMap<String, HashSet<String>>,
) -> usize {
    match orbiters_by_orbitee.get(body) {
        None => 0,
        Some(orbiters) => {
            let mut indirect_orbits = orbiters.len();
            for orbiter in orbiters {
                indirect_orbits += count_total_orbits(orbiter, orbiters_by_orbitee);
            }
            indirect_orbits
        }
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut orbiters_by_orbitee: HashMap<String, HashSet<String>> = HashMap::new();
    let mut direct_orbits = 0;
    for line in reader.lines() {
        match line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(orbit) => {
                let orbit_parts: Vec<&str> = orbit.splitn(3, ")").collect();
                if orbit_parts.len() != 2 {
                    return Err(format!("Invalid orbit: {}", orbit));
                }
                let orbitee = String::from(orbit_parts[0]);
                let orbiter = String::from(orbit_parts[1]);
                let orbiters = orbiters_by_orbitee.entry(orbitee).or_insert(HashSet::new());
                if orbiters.insert(orbiter) {
                    direct_orbits += 1;
                }
            }
        }
    }
    println!("Direct orbits: {}", direct_orbits);
    let mut total_orbits = 0;
    for orbitee in orbiters_by_orbitee.keys() {
        total_orbits += count_total_orbits(orbitee, &orbiters_by_orbitee);
    }
    println!("Total orbits: {}", total_orbits);
    Ok(())
}
