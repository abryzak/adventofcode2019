use std::io::BufRead;

const PATTERN: [i32; 4] = [0, 1, 0, -1];

struct PatternIterator {
    repeats: usize,
    pattern_idx: usize,
    remaining: usize,
}

impl PatternIterator {
    fn new(repeats: usize) -> Self {
        PatternIterator {
            repeats,
            pattern_idx: 0,
            remaining: repeats,
        }
    }
}

impl Iterator for PatternIterator {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.remaining == 0 {
            self.remaining = self.repeats;
            self.pattern_idx = (self.pattern_idx + 1) % PATTERN.len();
        }
        self.remaining -= 1;
        return Some(PATTERN[self.pattern_idx]);
    }
}

fn process(line: &str) -> Result<(), String> {
    let mut values: Vec<i32> = Vec::with_capacity(line.len());
    for i in 0..line.len() {
        let value: i32 = match line[i..i + 1].parse() {
            Err(err) => return Err(format!("Input contains non-number: {}", err)),
            Ok(v) => v,
        };
        values.push(value);
    }

    println!();
    println!("Input: {}", line);
    for phase in 1..=100 {
        let mut new_values: Vec<i32> = Vec::with_capacity(values.len());
        for digit_idx in 0..values.len() {
            let mut sum = 0;
            let mut pattern_iter = PatternIterator::new(digit_idx + 1).skip(1);
            for digit in &values {
                let multiplier = pattern_iter.next().expect("Infinite iterator stopped");
                sum += digit * multiplier;
            }
            let last_digit = sum.abs() % 10;
            new_values.push(last_digit);
        }
        values = new_values;
        if (phase >= 1 && phase <= 4) || phase == 100 {
            print!("After phase {}: ", phase);
            for value in &values {
                print!("{}", value);
            }
            println!();
        }
    }

    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                // TODO work out if this can be done without collecting
                process(line)?;
            }
        };
    }

    Ok(())
}
