use std::io::BufRead;

fn mass_to_fuel(mass: i32) -> i32 {
    mass / 3 - 2
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let total_fuel: i32 = reader.lines().try_fold(0, |total_fuel, line| match line {
        Err(err) => Err(format!("Error reading line: {}", err)),
        Ok(ref s) if s.is_empty() => Ok(total_fuel),
        Ok(s) => match s.parse() {
            Ok(mass) => Ok(total_fuel + mass_to_fuel(mass)),
            Err(err) => Err(format!(
                "Error parsing integer from line \"{}\": {}",
                s, err
            )),
        },
    })?;

    println!("Fuel required: {}", total_fuel);
    Ok(())
}
