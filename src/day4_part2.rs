use std::io::BufRead;

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut lines = reader.lines();
    let min: i32 = lines
        .next()
        .unwrap()
        .expect("expected min")
        .parse()
        .expect("Invalid number");
    let max: i32 = lines
        .next()
        .unwrap()
        .expect("expected max")
        .parse()
        .expect("Invalid number");
    let mut count = 0;
    for x in min..=max {
        let s = format!("{}", x);
        if only_increasing(&s) && has_double_not_in_larger_group(&s) {
            count += 1;
        }
    }
    println!("{} passwords possible", count);

    Ok(())
}

fn only_increasing(s: &String) -> bool {
    let mut last: char = ' ';
    for c in s.chars() {
        if c < last {
            return false;
        }
        last = c;
    }
    true
}

fn has_double_not_in_larger_group(s: &String) -> bool {
    let mut repeat_count = 0;
    let mut last: char = ' ';
    for c in s.chars() {
        if c == last {
            repeat_count += 1;
        } else {
            if repeat_count == 1 {
                return true;
            }
            repeat_count = 0;
        }
        last = c;
    }
    repeat_count == 1
}
