use std::collections::VecDeque;
use std::io::BufRead;

macro_rules! intcodedebug {
    () => {
        //        println!()
    };
    ($($arg:tt)*) => {{
        //        println!($($arg)*);
    }};
}

fn split_to_numbers<T: AsRef<str>>(s: T) -> Result<Vec<i64>, String> {
    let mut vals: Vec<i64> = Vec::new();
    for val in s.as_ref().split(',') {
        match val.parse() {
            Err(err) => return Err(format!("Unable to parse number \"{}\": {}", val, err)),
            Ok(v) => vals.push(v),
        }
    }
    Ok(vals)
}

enum ParameterMode {
    PositionMode,
    ImmediateMode,
    RelativeMode,
}

fn param_mode(mode_int: u32) -> Result<ParameterMode, String> {
    let mode = match mode_int {
        0 => ParameterMode::PositionMode,
        1 => ParameterMode::ImmediateMode,
        2 => ParameterMode::RelativeMode,
        _ => return Err(format!("Unrecognized parameter mode: {}", mode_int)),
    };
    Ok(mode)
}

enum ComputationResult {
    Finished,
    RequiresInput,
    Output(i64),
}

#[derive(Clone, Debug, PartialEq)]
struct Computer {
    ip: usize,
    relative_base: usize,
    memory: Vec<i64>,
    inputs: VecDeque<i64>,
}

impl Computer {
    fn validate_address(&self, address: i64) -> Result<usize, String> {
        if address < 0 {
            return Err(format!("Out of bounds memory access: {}", address));
        }
        Ok(address as usize)
    }

    fn read_mem(&self, address: usize) -> i64 {
        let value = if address >= self.memory.len() {
            0
        } else {
            self.memory[address]
        };
        intcodedebug!("read {}: {}", address, value);
        value
    }

    fn write_mem(&mut self, address: usize, value: i64) {
        // MAYBE store separate HashMap of extended addresses
        while address >= self.memory.len() {
            self.memory.push(0);
        }
        intcodedebug!("write {} to {}", value, address);
        self.memory[address] = value;
    }

    fn next_instruction(&self) -> Result<u32, String> {
        let instruction_value: i64 = self.read_mem(self.ip);
        if instruction_value < 0 {
            return Err(format!("Invalid instruction {}", instruction_value));
        }
        Ok(instruction_value as u32)
    }

    fn read_value_param(&self, param_idx: usize, instruction: u32) -> Result<i64, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                let address = self.validate_address(raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                self.read_mem(address)
            }
            ParameterMode::ImmediateMode => raw_value,
        };
        Ok(value)
    }

    fn read_address_param(&self, param_idx: usize, instruction: u32) -> Result<usize, String> {
        let raw_value = self.read_mem(self.ip + param_idx);
        if param_idx == 0 || param_idx > 3 {
            return Err(format!("Invalid param index: {}", param_idx));
        }
        let mode = param_mode(match param_idx {
            1 => (instruction / 100) % 10,
            2 => (instruction / 1000) % 10,
            3 => (instruction / 10000) % 10,
            _ => {
                return Err(format!("Invalid param index: {}", param_idx));
            }
        })?;
        let value = match mode {
            ParameterMode::PositionMode => {
                // MAYBE this should never happen?
                let address = self.validate_address(raw_value)?;
                //                self.validate_address(self.read_mem(address))?
                address
            }
            ParameterMode::RelativeMode => {
                let address = self.validate_address((self.relative_base as i64) + raw_value)?;
                address
            }
            ParameterMode::ImmediateMode => self.validate_address(raw_value)?,
        };
        Ok(value)
    }

    pub fn run(&mut self) -> Result<ComputationResult, String> {
        loop {
            intcodedebug!();
            let instruction = self.next_instruction()?;
            let opcode = instruction % 100;
            intcodedebug!(
                //                "IP: {}, instruction: {}, opcode: {}, rb: {}, param1: {}, param2: {}, param3: {}",
                "IP: {}, instruction: {}, opcode: {} rb: {}",
                self.ip,
                instruction,
                opcode,
                self.relative_base,
                //                self.read_mem(self.ip + 1),
                //                self.read_mem(self.ip + 2),
                //                self.read_mem(self.ip + 3)
            );
            match opcode {
                1 => {
                    // add 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 + val2);
                    self.ip += 4;
                }
                2 => {
                    // mul 1, 2, store at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    self.write_mem(address, val1 * val2);
                    self.ip += 4;
                }
                3 => {
                    // read input and store at address 1
                    let address = self.read_address_param(1, instruction)?;
                    intcodedebug!(
                        "address: {}, {}, relative base: {}",
                        address,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    let val = match self.inputs.pop_front() {
                        None => break Ok(ComputationResult::RequiresInput),
                        Some(v) => v,
                    };
                    self.write_mem(address, val);
                    self.ip += 2;
                }
                4 => {
                    // output 1
                    let val1 = self.read_value_param(1, instruction)?;
                    self.ip += 2;
                    break Ok(ComputationResult::Output(val1));
                }
                5 => {
                    // if 1 != 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 != 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                6 => {
                    // if 1 == 0, jump to address 2
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.validate_address(val2)?;
                    if val1 == 0 {
                        self.ip = address;
                    } else {
                        self.ip += 3;
                    }
                }
                7 => {
                    // if 1 < 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 < val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                8 => {
                    // if 1 = 2, store 1 at address 3, else store 0 at address 3
                    let val1 = self.read_value_param(1, instruction)?;
                    let val2 = self.read_value_param(2, instruction)?;
                    let address = self.read_address_param(3, instruction)?;
                    let val = if val1 == val2 { 1 } else { 0 };
                    self.write_mem(address, val);
                    self.ip += 4;
                }
                9 => {
                    // set relative base to relative base + 1
                    let val1 = self.read_value_param(1, instruction)?;
                    intcodedebug!(
                        "set relative base, instruction: {}, val: {}, raw_param: {}, relative base: {}",
                        instruction,
                        val1,
                        self.read_mem(self.ip + 1),
                        self.relative_base,
                    );
                    if val1 >= 0 {
                        self.relative_base += val1 as usize;
                    } else {
                        self.relative_base -= -val1 as usize;
                    }
                    self.ip += 2;
                }
                99 => break Ok(ComputationResult::Finished),
                _ => {
                    break Err(format!(
                        "Unexpected opcode {} at address {}",
                        opcode, self.ip
                    ))
                }
            };
        }
    }
}

// END COMPUTER

struct AsciiBot {
    computer: Computer,
    output: String,
}

impl AsciiBot {
    pub fn new(computer: Computer) -> Self {
        let ascii_bot = AsciiBot {
            computer,
            output: String::new(),
        };
        ascii_bot
    }

    pub fn run(&mut self) -> Result<(), String> {
        loop {
            match self.computer.run()? {
                ComputationResult::Finished => {
                    break;
                }
                ComputationResult::RequiresInput => {
                    return Err(format!("Expected input but none available"));
                }
                ComputationResult::Output(value) => match value {
                    10 => self.output.push('\n'),
                    13 => {}
                    0x20..=0x7e => self.output.push((value as u8) as char),
                    v => println!("Dust collected: {}", v),
                },
            }
        }

        println!("{}", self.output);

        Ok(())
    }

    pub fn wake_up(&mut self) {
        self.computer.memory[0] = 2;
    }

    pub fn provide_input(&mut self, input: &str) {
        for b in input.bytes() {
            self.computer.inputs.push_back(b as i64);
        }
        self.computer.inputs.push_back(10);
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let program = line.trim();
                if program.len() == 0 || program.starts_with("#") {
                    continue;
                }
                let base_memory = split_to_numbers(program)?;
                let computer = Computer {
                    ip: 0,
                    relative_base: 0,
                    memory: base_memory,
                    inputs: VecDeque::new(),
                };
                let mut ascii_bot = AsciiBot::new(computer);
                ascii_bot.wake_up();
                ascii_bot.provide_input("A,B,A,B,C,A,C,A,C,B");
                ascii_bot.provide_input("R,12,L,8,L,4,L,4");
                ascii_bot.provide_input("L,8,R,6,L,6");
                ascii_bot.provide_input("L,8,L,4,R,12,L,6,L,4");
                ascii_bot.provide_input("n");
                ascii_bot.run()?;
            }
        };
    }
    Ok(())
}
