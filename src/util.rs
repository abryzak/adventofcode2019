use std::env::Args;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Read};
use std::path::Path;

//pub fn single_path_from_args<'a, T: AsRef<str>>(args: &'a &[T]) -> Result<&'a Path, String> {
//    if args.len() != 1 {
//        return Err(String::from("No input path"));
//    }
//    return Ok(Path::new(args[0].as_ref()));
//}

pub fn reader_from_path<P: AsRef<Path>>(input_path: &P) -> Result<BufReader<File>, String> {
    let input_file = match File::open(input_path) {
        Err(err) => {
            return Err(format!(
                "Unable to open input file {}, {}",
                input_path.as_ref().display(),
                err
            ))
        }
        Ok(file) => file,
    };

    Ok(BufReader::new(input_file))
}

pub struct Input<'a> {
    source: Box<dyn BufRead + 'a>,
}

impl<'a> Read for Input<'a> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.source.read(buf)
    }
}

impl<'a> BufRead for Input<'a> {
    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        self.source.fill_buf()
    }

    fn consume(&mut self, amt: usize) {
        self.source.consume(amt);
    }
}

fn stdio_reader<'a>() -> Input<'a> {
    Input {
        source: Box::new(BufReader::new(io::stdin())),
    }
}

fn file_reader<'a, T: AsRef<str>>(file: T) -> Result<Input<'a>, String> {
    let reader = reader_from_path(&Path::new(file.as_ref()))?;
    Ok(Input {
        source: Box::new(reader),
    })
}

pub fn reader_from_args<'a>(mut args: Args) -> Result<Input<'a>, String> {
    match args.next() {
        Some(ref file) if file.as_str().eq("-") => Ok(stdio_reader()),
        Some(ref file) => file_reader(file),
        None => Ok(stdio_reader()),
    }
}
