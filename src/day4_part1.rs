use std::io::BufRead;

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut lines = reader.lines();
    let min: i32 = lines
        .next()
        .unwrap()
        .expect("expected min")
        .parse()
        .expect("Invalid number");
    let max: i32 = lines
        .next()
        .unwrap()
        .expect("expected max")
        .parse()
        .expect("Invalid number");
    let mut count = 0;
    for x in min..=max {
        let s = format!("{}", x);
        if only_increasing(&s) && has_double(&s) {
            count += 1;
        }
    }
    println!("{} passwords possible", count);

    Ok(())
}

fn only_increasing(s: &String) -> bool {
    let mut last: char = ' ';
    for c in s.chars() {
        if c < last {
            return false;
        }
        last = c;
    }
    true
}

fn has_double(s: &String) -> bool {
    let mut last: char = ' ';
    for c in s.chars() {
        if c == last {
            return true;
        }
        last = c;
    }
    false
}
