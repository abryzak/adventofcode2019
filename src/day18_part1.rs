use std::cmp::{max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::BufRead;

struct KeyDistance {
    pos: (usize, usize),
    key: char,
    distance: u32,
}

#[derive(Clone, Debug, PartialEq)]
enum Tile {
    Empty,
    Wall,
    Start,
    Key(char),
    Door(char),
}

#[derive(Clone, Debug, PartialEq)]
struct Maze {
    tiles: HashMap<(usize, usize), Tile>,
    max_x: usize,
    max_y: usize,
}

impl Maze {
    fn add_line_to_tiles(
        tiles: &mut HashMap<(usize, usize), Tile>,
        y: usize,
        line: &str,
    ) -> Result<(), String> {
        for (x, c) in line.chars().enumerate() {
            let tile = match c {
                '.' => Tile::Empty,
                '#' => Tile::Wall,
                '@' => Tile::Start,
                'a'..='z' => Tile::Key(c),
                'A'..='Z' => Tile::Door(c.to_ascii_lowercase()),
                _ => return Err(format!("Invalid character found in maze: {}", c)),
            };
            tiles.insert((x, y), tile);
        }
        Ok(())
    }

    fn breadth_first_search_for_keys(
        &self,
        start: (usize, usize),
    ) -> Result<Vec<KeyDistance>, String> {
        let mut visited: HashSet<(usize, usize)> = HashSet::new();
        visited.insert(start);
        let mut ends: VecDeque<((usize, usize), u32)> = VecDeque::new();
        ends.push_back((start, 0));
        let mut found_keys: Vec<KeyDistance> = Vec::new();

        while !ends.is_empty() {
            let ((x, y), distance) = ends.pop_front().unwrap();
            // println!("Checking {},{}: {:?}", x, y, self.tiles.get(&(x, y)));
            match self.tiles.get(&(x, y)) {
                None => return Err(format!("Expected tile at {},{} but found none", x, y)),
                Some(Tile::Start) => return Err(format!("Found second start tile at {},{}", x, y)),
                Some(Tile::Wall) => continue,
                Some(Tile::Door(_)) => continue,
                Some(Tile::Key(c)) => {
                    //                    println!("Found key {} at {},{}. Distance: {}", c, x, y, distance);
                    found_keys.push(KeyDistance {
                        pos: (x, y),
                        key: *c,
                        distance,
                    })
                }
                Some(Tile::Empty) => {}
            }
            // println!("Moving from {},{}", x, y);
            if x > 0 && visited.insert((x - 1, y)) {
                ends.push_back(((x - 1, y), distance + 1));
            }
            if x < self.max_x && visited.insert((x + 1, y)) {
                ends.push_back(((x + 1, y), distance + 1));
            }
            if y > 0 && visited.insert((x, y - 1)) {
                ends.push_back(((x, y - 1), distance + 1));
            }
            if y < self.max_y && visited.insert((x, y + 1)) {
                ends.push_back(((x, y + 1), distance + 1));
            }
        }

        Ok(found_keys)
    }

    fn clone_with_doors_unlocked(&self, key: char) -> Maze {
        let mut new_maze = self.clone();
        for (_, tile) in new_maze.tiles.iter_mut() {
            match &tile {
                Tile::Key(k) if *k == key => *tile = Tile::Empty,
                Tile::Door(k) if *k == key => *tile = Tile::Empty,
                _ => {}
            };
        }
        new_maze
    }

    fn print(&self) {
        for y in 0..=self.max_y {
            for x in 0..=self.max_x {
                let c = match self.tiles[&(x, y)] {
                    Tile::Empty => ' ',
                    Tile::Wall => '█',
                    Tile::Start => '@',
                    Tile::Key(c) => c,
                    Tile::Door(c) => c.to_ascii_uppercase(),
                };
                print!("{}", c);
            }
            println!();
        }
    }

    fn solve_recurse(
        &self,
        start: (usize, usize),
        total_distance: u32,
        order_of_keys: &Vec<char>,
    ) -> Result<(u32, Vec<char>), String> {
        let found_keys = self.breadth_first_search_for_keys(start)?;
        if found_keys.is_empty() {
            // println!("Total distance for {:?}: {}", order_of_keys, total_distance);
            return Ok((total_distance, order_of_keys.clone()));
        }
        let mut min_distance = std::u32::MAX;
        let mut min_order_of_keys: Option<Vec<char>> = None;
        for found_key in found_keys {
            let new_maze = self.clone_with_doors_unlocked(found_key.key);
            let mut new_order_of_keys = order_of_keys.clone();
            new_order_of_keys.push(found_key.key);
            let (distance, result_order_of_keys) = new_maze.solve_recurse(
                found_key.pos,
                total_distance + found_key.distance,
                &new_order_of_keys,
            )?;
            if distance < min_distance {
                min_distance = distance;
                min_order_of_keys = Some(result_order_of_keys);
            }
            min_distance = min(min_distance, distance);
        }
        return Ok((min_distance, min_order_of_keys.unwrap()));
    }

    pub fn solve(&self) -> Result<(), String> {
        let mut new_maze = self.clone();
        let start: (usize, usize) = match new_maze.tiles.iter().find(|(_, tile)| match &tile {
            Tile::Start => true,
            _ => false,
        }) {
            None => return Err(format!("No start location found!")),
            Some(((x, y), _)) => (*x, *y),
        };
        new_maze.tiles.insert(start, Tile::Empty);
        let order_of_keys: Vec<char> = Vec::new();
        let (min_distance, min_order_of_keys) = new_maze.solve_recurse(start, 0, &order_of_keys)?;
        println!("Minimum distance to get all keys is {}", min_distance);
        println!("Order of keys: {:?}", min_order_of_keys);

        Ok(())
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut tiles: HashMap<(usize, usize), Tile> = HashMap::new();

    let mut max_x = 0;
    let mut max_y = 0;

    for (y, read_line) in reader.lines().enumerate() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                max_x = max(max_x, line.len() - 1);
                Maze::add_line_to_tiles(&mut tiles, y, line)?;
            }
        };
        max_y = y;
    }

    let maze = Maze {
        tiles,
        max_x,
        max_y,
    };
    maze.solve()?;

    Ok(())
}
