mod day10_part1;
mod day10_part2;
mod day11_part1;
mod day11_part2;
mod day12_part1;
mod day12_part2;
mod day13_part1;
mod day13_part2;
mod day14_part1;
mod day14_part2;
mod day15_part1;
mod day15_part2;
mod day16_part1;
mod day16_part2;
mod day17_part1;
mod day17_part2;
mod day18_part1;
mod day19_part1;
mod day19_part2;
mod day1_part1;
mod day1_part2;
mod day2_part1;
mod day2_part2;
mod day3_part1;
mod day3_part2;
mod day4_part1;
mod day4_part2;
mod day5_part1;
mod day5_part2;
mod day6_part1;
mod day6_part2;
mod day7_part1;
mod day7_part2;
mod day8_part1;
mod day8_part2;
mod day9_part1;
mod day9_part2;
mod util;

extern crate num_integer;
extern crate regex;

use std::env;
use std::env::Args;
use std::process;
use std::time::Instant;

fn main() {
    let mut args: Args = env::args();

    let executable_name = match args.next() {
        Some(v) => v,
        None => {
            eprintln!("No executable specified");
            process::exit(1);
        }
    };

    let problem_name = match args.next() {
        Some(v) => v,
        None => {
            eprintln!("Usage: {} problem_name", executable_name);
            process::exit(1);
        }
    };

    let start = Instant::now();

    match problem_name.as_str() {
        "day1_part1" => util::reader_from_args(args).and_then(day1_part1::solve),
        "day1_part2" => util::reader_from_args(args).and_then(day1_part2::solve),
        "day2_part1" => util::reader_from_args(args).and_then(day2_part1::solve),
        "day2_part2" => util::reader_from_args(args).and_then(day2_part2::solve),
        "day3_part1" => util::reader_from_args(args).and_then(day3_part1::solve),
        "day3_part2" => util::reader_from_args(args).and_then(day3_part2::solve),
        "day4_part1" => util::reader_from_args(args).and_then(day4_part1::solve),
        "day4_part2" => util::reader_from_args(args).and_then(day4_part2::solve),
        "day5_part1" => util::reader_from_args(args).and_then(day5_part1::solve),
        "day5_part2" => util::reader_from_args(args).and_then(day5_part2::solve),
        "day6_part1" => util::reader_from_args(args).and_then(day6_part1::solve),
        "day6_part2" => util::reader_from_args(args).and_then(day6_part2::solve),
        "day7_part1" => util::reader_from_args(args).and_then(day7_part1::solve),
        "day7_part2" => util::reader_from_args(args).and_then(day7_part2::solve),
        "day8_part1" => util::reader_from_args(args).and_then(day8_part1::solve),
        "day8_part2" => util::reader_from_args(args).and_then(day8_part2::solve),
        "day9_part1" => util::reader_from_args(args).and_then(day9_part1::solve),
        "day9_part2" => util::reader_from_args(args).and_then(day9_part2::solve),
        "day10_part1" => util::reader_from_args(args).and_then(day10_part1::solve),
        "day10_part2" => util::reader_from_args(args).and_then(day10_part2::solve),
        "day11_part1" => util::reader_from_args(args).and_then(day11_part1::solve),
        "day11_part2" => util::reader_from_args(args).and_then(day11_part2::solve),
        "day12_part1" => util::reader_from_args(args).and_then(day12_part1::solve),
        "day12_part2" => util::reader_from_args(args).and_then(day12_part2::solve),
        "day13_part1" => util::reader_from_args(args).and_then(day13_part1::solve),
        "day13_part2" => util::reader_from_args(args).and_then(day13_part2::solve),
        "day14_part1" => util::reader_from_args(args).and_then(day14_part1::solve),
        "day14_part2" => util::reader_from_args(args).and_then(day14_part2::solve),
        "day15_part1" => util::reader_from_args(args).and_then(day15_part1::solve),
        "day15_part2" => util::reader_from_args(args).and_then(day15_part2::solve),
        "day16_part1" => util::reader_from_args(args).and_then(day16_part1::solve),
        "day16_part2" => util::reader_from_args(args).and_then(day16_part2::solve),
        "day17_part1" => util::reader_from_args(args).and_then(day17_part1::solve),
        "day17_part2" => util::reader_from_args(args).and_then(day17_part2::solve),
        "day18_part1" => util::reader_from_args(args).and_then(day18_part1::solve),
        "day19_part1" => util::reader_from_args(args).and_then(day19_part1::solve),
        "day19_part2" => util::reader_from_args(args).and_then(day19_part2::solve),
        _ => Err(format!("Unknown problem: {}", problem_name)),
    }
    .unwrap_or_else(|err| {
        eprintln!("Problem {} failed with error: {}", problem_name, err);
        process::exit(2);
    });

    println!("Took {}s", start.elapsed().as_secs_f32());
}
