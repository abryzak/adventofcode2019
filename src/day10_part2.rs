use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::f64;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq, PartialOrd)]
struct AsteroidAngle(f64);

impl Eq for AsteroidAngle {}

impl Ord for AsteroidAngle {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.0 > other.0 {
            return Ordering::Greater;
        }
        if self.0 < other.0 {
            return Ordering::Less;
        }
        return Ordering::Equal;
    }
}

enum Entity {
    Space,
    Asteroid,
}

struct Map {
    entities: Vec<Entity>,
    cols: usize,
}

impl Map {
    pub fn new(cols: usize) -> Map {
        let entities = Vec::with_capacity(cols * cols);
        return Map {
            entities: entities,
            cols,
        };
    }

    pub fn add_row(&mut self, line: &str) -> Result<(), String> {
        if line.len() != self.cols {
            return Err(format!(
                "Different number of columns in row: {}, should be {}",
                line.len(),
                self.cols
            ));
        }
        let mut row = Vec::with_capacity(self.cols);
        for c in line.chars() {
            match c {
                '.' => row.push(Entity::Space),
                '#' => row.push(Entity::Asteroid),
                c => return Err(format!("Unsupported entity: {}", c)),
            }
        }
        self.entities.append(&mut row);
        Ok(())
    }

    fn visible_asteroids_quadrant(
        &self,
        visible_asteroids: &mut Vec<(usize, usize)>,
        rows: usize,
        origin_col: usize,
        origin_row: usize,
        mask: &mut Vec<bool>,
        col_delta_delta: isize,
        row_delta_delta: isize,
    ) -> () {
        let cols = self.cols;
        let mut row_delta: isize = 0;
        while (row_delta.abs() as usize) < rows {
            let mut col_delta: isize = 0;
            while (col_delta.abs() as usize) < cols {
                if row_delta != 0 || col_delta != 0 {
                    let mut found = false;
                    let mut row = (origin_row as isize) + row_delta;
                    let mut col = (origin_col as isize) + col_delta;
                    while row >= 0 && (row as usize) < rows && col >= 0 && (col as usize) < cols {
                        let idx = (row as usize) * cols + (col as usize);
                        if !mask[idx] {
                            mask[idx] = true;
                            match self.entities[idx] {
                                Entity::Asteroid => {
                                    if !found {
                                        visible_asteroids.push((col as usize, row as usize));
                                        found = true;
                                    }
                                }
                                Entity::Space => (),
                            }
                        }
                        row += row_delta;
                        col += col_delta;
                    }
                }
                col_delta += col_delta_delta;
            }
            row_delta += row_delta_delta;
        }
    }

    fn visible_asteroids(&self, origin_col: usize, origin_row: usize) -> Vec<(usize, usize)> {
        let rows = self.entities.len() / self.cols;
        let mut visible_asteroids: Vec<(usize, usize)> = Vec::new();
        let mut mask = vec![false; self.entities.len()];
        self.visible_asteroids_quadrant(
            &mut visible_asteroids,
            rows,
            origin_col,
            origin_row,
            &mut mask,
            -1,
            -1,
        );
        self.visible_asteroids_quadrant(
            &mut visible_asteroids,
            rows,
            origin_col,
            origin_row,
            &mut mask,
            -1,
            1,
        );
        self.visible_asteroids_quadrant(
            &mut visible_asteroids,
            rows,
            origin_col,
            origin_row,
            &mut mask,
            1,
            -1,
        );
        self.visible_asteroids_quadrant(
            &mut visible_asteroids,
            rows,
            origin_col,
            origin_row,
            &mut mask,
            1,
            1,
        );
        visible_asteroids
    }

    pub fn solve(&self) -> Result<(), String> {
        // MAYBE make this solve part 1 to get best co-ordinates
        let origin_col = 19;
        let origin_row = 11;
        let visible_asteroids = self.visible_asteroids(origin_col, origin_row);
        let mut visible_asteroid_by_angle = BTreeMap::new();
        for visible_asteroid in visible_asteroids {
            let (col, row) = visible_asteroid;
            let delta_col = (col as f64) - (origin_col as f64);
            let delta_row = (origin_row as f64) - (row as f64); // y is positive up
            let mut angle = delta_row.atan2(delta_col);
            // println!("Raw angle for {}x{}: {}", col, row, angle);
            angle = f64::consts::FRAC_PI_2 - angle;
            if angle < 0.0 {
                angle += 2.0 * f64::consts::PI;
            }
            visible_asteroid_by_angle.insert(AsteroidAngle { 0: angle }, visible_asteroid);
        }
        // for (angle, (col, row)) in &visible_asteroid_by_angle {
        //     println!("{}x{} angle {}", col, row, angle.0);
        // }
        match visible_asteroid_by_angle.iter().skip(199).next() {
            None => return Err(format!("Less than 200 visible asteroids")),
            Some(visible_asteroid) => {
                let (angle, (col, row)) = visible_asteroid;
                println!(
                    "200th visible asteroid is at {}x{} angle: {}",
                    col, row, angle.0
                )
            }
        }
        Ok(())
    }
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    let mut lines = reader.lines();
    let mut map = match lines.next() {
        None => return Err(format!("Missing first line")),
        Some(read_line) => match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let mut map = Map::new(line.len());
                map.add_row(line)?;
                map
            }
        },
    };
    for read_line in lines {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                map.add_row(line)?;
            }
        };
    }
    map.solve()
}
