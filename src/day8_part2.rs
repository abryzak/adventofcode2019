use std::io::BufRead;

fn solve_puzzle(line: &str, width: usize, height: usize) -> Result<(), String> {
    let mut layers: Vec<&str> = Vec::new();
    let mut i = 0;
    while i < line.len() {
        let end = i + (width * height);
        if end > line.len() {
            return Err(format!("Incomplete layer"));
        }
        layers.push(&line[i..end]);
        i = end;
    }
    let mut output_layer: Vec<char> = Vec::with_capacity(width * height);
    for _ in 0..width * height {
        output_layer.push('2');
    }
    for layer in layers {
        for i in 0..width * height {
            if output_layer[i] == '2' {
                output_layer[i] = layer.chars().nth(i).unwrap();
            }
        }
    }
    for row in 0..height {
        let start = row * width;
        let end = start + width;
        let mut row = String::new();
        for i in start..end {
            let s = match output_layer[i] {
                '0' => " ",
                '1' => "█",
                '2' => "_",
                c => return Err(format!("Invalid character in output: {}", c)),
            };
            row.push_str(s);
        }
        println!("{}", row);
    }
    Ok(())
}

pub fn solve<T: BufRead>(reader: T) -> Result<(), String> {
    solve_puzzle("0222112222120000", 2, 2)?;
    for read_line in reader.lines() {
        match read_line {
            Err(err) => return Err(format!("Error reading line: {}", err)),
            Ok(ref line) => {
                let trimmed = line.trim();
                if trimmed.len() == 0 || trimmed.starts_with("#") {
                    continue;
                }
                solve_puzzle(trimmed, 25, 6)?;
            }
        };
    }
    Ok(())
}
